/**
 * o.url              {String}        接口网址
 * o.data             {Object}        接口请求参数
 * o.jsonpCallback    {String}        接口回调函数名   name 固定的函数名  name{n}函数名后跟递增数字  name{t}函数名后跟时间戳 {N和T大小写均可}
 * o.timeout          {Number}        接口请求超时时间
 *
 * 插件使用了Promise对象，请自行配置Promise打包支持。
 *
 * V1.0.1
 * */

let count = 0;
let mcJsonp = function (o) {
  return new Promise((resolve, reject) => {
    if (o && o.url) {
      setTimeout(function () {
        let jsonpCallback =
          (o.jsonpCallback &&
            o.jsonpCallback.replace(/\{[ntNT]\}/, function (str) {
              return str === '{n}' || str === '{N}' ? count++ : str === '{t}' || str === '{T}' ? new Date().getTime() : '';
            })) ||
          'JSONP' + count++;

        let jsonp = o.jsonp || 'callback';

        let timeout = o.timeout || 7000;
        let target = document.getElementsByTagName('script')[0] || document.head;
        let script;
        let timer;

        function serialize(data) {
          return Object.keys(data)
            .map(function (key) {
              return key + '=' + data[key];
            })
            .join('&');
        }

        let url = o.url + (/\?/.test(o.url) ? '&' : '?') + serialize(o.data || {});

        function cleanup() {
          if (script.parentNode) script.parentNode.removeChild(script);

          try {
            delete window[jsonpCallback];
          } catch (e) {
            window[jsonpCallback] = null;
          }
          if (timer) clearTimeout(timer);
        }

        window[jsonpCallback] = (data) => {
          cleanup();
          resolve(data);
        };

        if (timeout) {
          timer = setTimeout(() => {
            cleanup();
            reject(new Error('Timeout'));
          }, timeout);
        }

        // add qs component
        url += (~url.indexOf('?') ? '&' : '?') + jsonp + '=' + encodeURIComponent(jsonpCallback);
        url = url.replace('?&', '?');

        // 创建一个script元素
        script = document.createElement('script');
        script.src = url;
        script.onerror = () => reject(new Error('ERR: SCRIPT_LOADING_ERROR'));
        target.parentNode.insertBefore(script, target);
      }, 1);
    } else {
      console.warn('ERR: MISSING_PASS_ARGUMENT');
    }
  });
};

module.exports = mcJsonp;
