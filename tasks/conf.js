module.exports = {
    dev: {
        html: {
            out : '',                    // HTML输出目录
            min : false,                 // 是否压缩
            mix : false,                 //
            bare: false                  // 是否去除<HTML>,<BODY><HEAD>等标签
        },
        css : {
            out   : 'css',               // CSS输出目录
            syntax: 'scss',              // 编译语言
            min   : true,                // 是否压缩
            map   : true                 // 是否生成MAP源文件追踪
        },
        js  : {
            out   : 'js',                // JS输出目录
            syntax: 'es5',               // 编译语言
            min   : true,                // 是否压缩
            map   : true,                // 是否生成MAP源文件追踪
            cmd   : false                // 是否为CMD模式打包(CMD采用webpack打包)
        },
        img : {
            out   : 'img',               // 图片输出目录
            min   : false,               // 是否压缩图片
            sprite: false,               // 是否开启雪碧图 - 功能未建设
            base64: false                // 是否把图片转为BASE64码
        },
        out : 'dist'                     // 整个项目打包输出目录
    },
    pro: {
        html: {
            out : '',
            min : true,
            mix : false,
            bare: false
        },
        css : {
            out   : 'css',
            syntax: 'scss',
            min   : true,
            map   : false
        },
        js  : {
            out   : 'js',
            syntax: 'es5',
            min   : true,
            map   : false,
            cmd   : false
        },
        img : {
            out   : 'img',
            min   : true,
            sprite: false,
            base64: false
        },
        out : 'dist'
    }
};