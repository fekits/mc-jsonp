# MC-JSONP

```$xslt
一个轻量的JSONP接口请求插件，用法和属性名和jquery的ajax请求jsonp差不多。
```

#### 索引

- [演示](#演示)
- [参数](#参数)
- [示例](#示例)
- [版本](#版本)
- [反馈](#反馈)

#### 演示

[https://fekit.asnowsoft.com/plugins/mc-jsonp](https://fekit.asnowsoft.com/plugins/mc-jsonp/)

#### 开始

下载项目:

```npm
npm i @fekit/mc-jsonp
```

#### 参数

```$xslt
param                  {Object}        参数
param.url              {String}        接口网址
param.data             {Object}        接口请求参数
param.jsonpCallback    {String}        接口回调函数名   name 固定的函数名  name{n}函数名后跟递增数字  name{t}函数名后跟时间戳 {N和T大小写均可}
param.timeout          {Number}        接口请求超时时间
```

#### 示例

```javascript

import mcJsonp from './mc-jsonp';

let myData = mcJsonp({
  url: 'http://baike.baidu.com/api/openapi/BaikeLemmaCardApi',
  jsonpCallback: 'JSONP{N}',
  data: {
      scope: '103',
      format: 'json',
      appid: '379020',
      bk_key: bk_key,
      bk_length: 100
  }
});
myData.then(function(res){
  console.log(res)
})


// 多个请求
let aaa = mcJsonp({
  url: 'http://baike.baidu.com/api/openapi/BaikeLemmaCardApi',
  jsonpCallback: 'JSONP{N}',
  data: {
    scope: '103',
    format: 'json',
    appid: '379020',
    bk_key: '前端',
    bk_length: 100
  }
});

let bbb = mcJsonp({
  url: 'http://baike.baidu.com/api/openapi/BaikeLemmaCardApi?scope=103&format=json&appid=379020&bk_length=100',
  jsonpCallback: 'JSONP{N}',
  data: {
    bk_key: '开发'
  }
});

Promise.all([aaa, bbb]).then(function ([aaa, bbb]) {
  console.log(aaa, bbb);
});
```

#### 版本
```$xslt
v1.0.2 [Latest version]
1. 修复一些文档编写错识
```

```$xslt
v1.0.1
1. 修复一个无任何入参时拼接参数出错的BUG
```

```$xslt
v1.0.0
1. 核心功能完成。
```

#### 反馈

```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
