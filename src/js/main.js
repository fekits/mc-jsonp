import '../css/demo.scss';
import McTinting from '@fekit/mc-tinting';
import mcJsonp from '../../lib/mc-jsonp';

new McTinting({ theme: 'chrome' });

let toSearch = document.getElementById('to_search');

toSearch.onclick = function () {
  let bk_key = document.getElementById('bk_key').value;
  let bbb = mcJsonp({
    url: 'http://baike.baidu.com/api/openapi/BaikeLemmaCardApi',
    jsonpCallback: 'JSONP{N}',
    data: {
      scope: '103',
      format: 'json',
      appid: '379020',
      bk_key: bk_key,
      bk_length: 100,
    },
  });

  bbb.then(function ([aaa, bbb]) {
    console.log(aaa, bbb);
  });
};
